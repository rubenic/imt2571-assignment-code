<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db) {
			$this->db = $db;
		}
		else {
            // Create PDO connection
      try {
        $this->db = new PDO('mysql:host=localhost;dbname=RubensBase;charset=utf8mb4','root','');

      } catch(PDOException $e) {
          print $e->getMessage();
        }
		}
  }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList() {
		    $booklist = array();
        try{

         $tempA = $this->db->query('SELECT * FROM Book');


          foreach($tempA as $book) {
            $booklist[] = new Book(
                        $book['title'],
                        $book['author'],
                        $book['description'],
                        $book['id']
                        );
          }

        }
        catch(PDOException $ex) {
          print $ex->getMessage();
        }
        return $booklist;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;

        // henter bok som tilhører id fra database.
        // hvis den finnes opprett et nytt bok objekt og oppdater attributtene og returner boken
        //hvis ikke den finnes returner null
        try{
          $stmt = $this->db->prepare("SELECT * FROM Book WHERE id=?");
          $stmt->bindValue(1,$id, PDO::PARAM_INT);
          $stmt->execute();
          $row = $stmt->fetch(PDO::FETCH_ASSOC);

          if($row != '') {
            $book = new Book(
                      $row['title'],
                      $row['author'],
                      $row['description'],
                      $row['id']
              );

          } else {

            print 'Did not find this book';
            return null;

          }
        }
        catch(PDOExeption $ex) {
          echo $ex->getMessage();
        }


        return $book;
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book){
      try{
        if($book->author != '' && $book->title != ''){
          if($book->description == ''){ $book->description = null;}
          $stmt = $this->db->prepare("INSERT INTO Book(title, author, description) VALUES(?,?,?)");
          $stmt->execute(array($book->title,$book->author,$book->description));
        }
        else{
          $view = new ErrorView();
          $view->create();
        }
      }
      catch(PDOExeption $ex){
        print $ex->getMessage();
      }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      try{
        $stmt = $this->db->prepare("UPDATE Book SET title=?, author=?, description=? WHERE id=?");
        if($book->author != '' && $book->title != ''){
          if($book->description == ''){ $book->description = null;}
          $stmt->execute(array($book->title,$book->author,$book->description, $book->id));
        }
        else{
          $view = new ErrorView();
          $view->create();
        }
      }
      catch(PDOExeption $ex) {
        print $ex->getMessage();
      }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      try{
        $stmt = $this->db->prepare("DELETE FROM Book WHERE id=:id");
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
      }
      catch(PDOExeption $ex) {
        print $ex->getMessage();
      }
    }

}

?>
